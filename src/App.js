import { useState, useEffect } from "react";
import "./App.css";

function App() {
    const cellWidth = 30;
    const [width, setWidth] = useState(0);
    const [data, setData] = useState([]);
    const [result, setResult] = useState([]);
    const [levels, setLevels] = useState([]);
    const [level, setLevel] = useState(0);
    
    useEffect(() => {
        fetch('https://demo7919674.mockable.io/')
            .then(response => response.json())
            .then(data => setLevels(data));
    }, [])

    const onChangeLevel = (e) => {
        setLevel(e.target.value);
    }

    const onSubmitLevel = (e) => {
        e.preventDefault();
        setData(Array.from(Array(level * level).keys()).map(item => ({id: item, active: false})));
        setWidth(cellWidth * level + 'px');
        setResult([]);
    }

    const onMouseEnterOnCell = (id, active) => {
        const changedData = data.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    active: !item.active
                }
            }

            return {...item}
        })
        setData(changedData);

        if (active) {
            const resultAfterAction = result.filter(item => item.id !== id);
            setResult(resultAfterAction);
        } else {
            const row = Math.floor(id / level) + 1;
            const col = id % level + 1;
            const resultItem = {row, col, id};
            const resultAfterAction = [...result, resultItem];

            setResult(resultAfterAction);
        }
    }

    return <div className="container">
        <div className="row justify-content-center mb-3">
            <div className="col-lg-6">
                <form className="d-flex"
                    onSubmit={(e) => onSubmitLevel(e)}>
                    <select className="form-control me-3"
                        value={level}
                        onChange={(e) => onChangeLevel(e)}>
                        <option value="0">
                            Pick mode
                        </option>
                        {
                            levels.map(item =>
                                <option key={item.name}
                                    value={item.field}>
                                    {item.name}
                                </option>
                            )
                        }
                    </select>
                    <button className="btn btn-primary">
                        Start
                    </button>
                </form>
            </div>
        </div>
        <div className="row">
            <div className="col">
                <div className="d-flex justify-content-center">
                    <div className="line me-3"
                        style={{width}}>
                        {
                            data.map((item) =>
                                <div key={item.id}
                                    className={item.active ? 'active cell' : 'cell'}
                                    onMouseEnter={() => onMouseEnterOnCell(item.id, item.active)}></div>
                            )
                        }
                    </div>
                    <div className="result">
                        {
                            result.map((item) =>
                                <div key={item.id}
                                    className="alert alert-warning p-1 mb-1">
                                    row {item.row} col {item.col}
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>;
}

export default App;
